import numpy as np
import matplotlib.pyplot as plt
import sympy as sp
#BUEN TRABAJO
#5.0


class Integral_MonteCarlo():

    def __init__(self,Function,Limit_min,Limit_max,N=1000):

        # Inicializamos Atributos
        self.a = Limit_min
        self.b = Limit_max
        self.N = int(N)
        self.fun = Function
        self.X = np.random.uniform(self.a,self.b,self.N)
        self.Y = self.fun(self.X)
    
    # Integral Exacta, utilizando sympy
    def Integral_sp(self):

        x_sp = sp.Symbol('x') # x Simbólico
        # Se define la función simbólica
        def fun_sp(x): 
            return x**2*sp.cos(x) 

        # Se calcula la integral
        Int_sp = sp.integrate(fun_sp(x_sp),(x_sp,self.a,self.b))
        return Int_sp

    # Integral por el método de Monte Carlo
    def Integral_Area(self):

        # Rectangulo que contiene la función
        Max = self.Y.max()
        Min = self.Y.min()
        
        # Valores de Y aleatorios
        Ys = np.random.uniform(Min,Max,self.N)

        # Máscaras Booleanas
        Y_positives = Ys >= np.zeros(self.N) # Para función sobre el eje X
        Y_less = Ys <= self.fun(self.X)
        IsInside_positive = Y_positives & Y_less

        Y_negatives = Ys <= np.zeros(self.N) # Para función bajo el eje X
        Y_greater = Ys >= self.fun(self.X)     
        IsInside_negative = Y_negatives & Y_greater
        
        # Se cuentan los puntos que están dentro de la función
        Conteo = (IsInside_positive.sum()-IsInside_negative.sum())/self.N 
        return Conteo*(Max-Min)*(self.b-self.a) # Resultado de la integral
    
    # Graficando la convergencia al valor de la integral
    def Grafica(self,Nmax = 10000,NPoints = 100):

        # Calculando la integral exacta
        Int_exac = self.Integral_sp()

        plt.figure(figsize=(10,5))

        # Graficando la integral exacta
        plt.hlines(Int_exac,0,Nmax,color='b',label='Integral Exacta')


        Iters = np.linspace(2,Nmax,NPoints)
        Values = []

        for i in Iters:
            # Valor de la integral para un número N de iteraciones
            self.N = int(i)
            self.X = np.random.uniform(self.a,self.b,self.N)
            self.Y = self.fun(self.X)
            Int = self.Integral_Area()

            Values.append(Int)

        
        plt.scatter(Iters,Values,color='orange',label='Integral Monte Carlo')
        
        plt.title('Integral Monte Carlo vs Integral Exacta')
        plt.xlabel('N')
        plt.ylabel('Resultado')
        plt.grid()
        plt.legend()
        
        plt.savefig('Plot_Integral_MonteCarlo.png')


