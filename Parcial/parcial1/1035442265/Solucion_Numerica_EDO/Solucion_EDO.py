import numpy as np 
import sympy as sp
import matplotlib.pyplot as plt


class SolucionED():
    def __init__(self,ys,xs,fs,xi,yi,):
        
        self.xs =xs
        self.ys = ys
        self.fs=fs
        self.xi=xi
        self.yi=yi

    #def EcuacionDiferencial(self):
     #   return sp.Eq(self.ys(self.xs).diff(self.xs),(self.fs))
    def EcuacionDiferencial(self):
        sol_ed= sp.dsolve(self.ys(self.xs).diff(self.xs)-self.fs)  
        c_ed =sp.Eq(sol_ed.lhs.subs(self.ys(self.xs),self.yi), sol_ed.rhs.subs(self.xs,self.xi)) 
        const_c = sp.solve(c_ed)
        fun = -sp.exp(-self.xs)
        return fun


        
    
    def a_X(self):
        x = np.arange(0,1,0.01)
        return x
    
    def a_y(self):
        
        f1 = sp.lambdify(self.xs,self.EcuacionDiferencial(),'numpy')
        y =[f1(i) for i in self.a_X()]

        return y
    
    def FigMap(self):
        plt.figure(figsize=[10,8])
        plt.plot(self.a_X(),self.a_y())
        plt.savefig("Solucion_analitica.png")

class solNumerica(SolucionED):
    def __init__(self,a,b,n,y0,f,point=False) :
        print("Inizializando soluciones numericas")
       
        self.a = a
        self.b = b
        self.n =n
        self.y0=y0
        self.f=f

        self.x = []
        self.y = []
        self.point = point

    def X(self):
        if self.point:
            self.b = self.point
            self.x = np.arange(self.a,self.point,self.h())

        else :
            self.x = np.arange(self.a,self.b,self.h())

    def h(self):
        return (self.b- self.a)/self.n
    
    def Euler(self):

        self.X()
        #self.x=np.arrange(self.a,self.b,self.h)
        self.y =[self.y0]

        for i in range(self.n):
            self.y.append(self.y[i] + self.h() * self.f(self.x[i],self.y[i]))

            return self.x, self.y
        
    def FigMap2(self):

        plt.figure(figsize=[10,8])
        plt.plot(self.Euler())
        plt.savefig("Numerica.png")