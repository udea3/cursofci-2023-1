from PenduloBalisitco import PenduloBalistico
from PenduloBalisitco import PooPendulo

'''Execution para la implementación de la clase del péndulo balístico
La clase recibe como parametros la longitud del péndulo, la masa de la bala, la masa del bloque,
y la velocidad inicial de la bala'''

if __name__ == "__main__":
    l =0.5  # m
    m =0# 0.2  # kg
    M = 10  # kg
    v0 = 225  # m/s
    g = 9.8
    theta = 120

    sol = PenduloBalistico(l, m, M, v0, g)
    print('la velocidad inicial de la bala es', v0, 'm/s')
    print('la masa de la vala y del bloque: ', m, 'kg', 'y',  M, 'kg')
    print('para estas condiciones iniciales, el ángulo de desviación es {}'.format(sol.angulo()), '°')
    print('la velocidad inicial del conjunto bala-bloque es: {}'.format(sol.vel_c()), 'm/s')
    print('===================================================')

    sol_vmin = PooPendulo(l, m, M, v0, g, theta)
    print('la velocidad mínima del conjunto bala-bloque para que de una vuelta con estos atributos es: {}'.format(sol_vmin.v_min()), 'm/s')
    print('la velocidad mínima de la bala para que de una vuelta con estos atributos es: {}'.format(sol_vmin.vmin_b()), 'm/s')
    print('===================================================')

    print('para un ángulo de desviación de:', theta, '°', 'la velocidad bloque - bala debe ser de', sol_vmin.v_angulo(), 'm/s')

    # finalmente graficamos la oscilación del péndulo balístico
    print(sol_vmin.graficar())