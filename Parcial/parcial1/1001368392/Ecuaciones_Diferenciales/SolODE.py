import numpy as np
from sympy import Function, dsolve, Derivative, lambdify, symbols
import matplotlib.pyplot as plt
from sympy.abc import x, y 
class SolODE():
    """
    Este clase es capaz de solucionar ecuaciones diferenciales ordinarios de primer orden de manera 
    analitica usando el sympy y de manera numerica usando el metodo de Euler. Junto a esto se proveen
    metodos para graficar ambas soluciones.

    Los parametros necesarios para definir la clase son:
        - Derivada de la funcion dy/dx en terminos de y como funcion de x en sympy y x como simbolo 
        de sympy: dyfun
        - Derivada de la funcion dy/dx en terminos de y como simbolo en sympy y x como simbolo 
        de sympy: dysym
        - Valor inicial de x: x0
        - Valor inicial de y: y0
        - Valor final de x: xf
        - Numero de iteraciones del metodo de Euler: n (Entero positivo)
    
    """
    def __init__(self, dyfun, dysym, x0, y0, xf, n):
        y = Function('y')
        self.f = Derivative(y(x), x)  - dyfun
        self.dy = dysym
        self.x0 = x0
        self.y0 = y0
        self.n = n
        self.xf = xf

    # Solucion exacta dada como equality de sympy:
    def exactsol(self):
        y = Function('y')
        ics = {y(self.x0): self.y0}
        sol = dsolve(self.f, y(x), ics=ics)
        return sol

    # Salto en x del metodo de Euler:
    def h(self):
        if self.xf <= self.x0:
            raise Exception("Ingrese un valor final mayor al inicial")
        return (self.xf - self.x0)/self.n
    
    # Arreglo de X dados los puntos finales, iniciales, h y el numero de iteraciones
    def arrX(self):
        if self.xf <= self.x0:
            raise Exception("Ingrese un valor final mayor al inicial")
        h = self.h()
        arrx = np.linspace(self.x0, self.xf + h, self.n)
        return arrx

    # Metodo de Euler para la integracion de la ecuacion en el intervalo dado
    def inteuler(self):
        h = self.h()
        xarr = self.arrX()
        yarr = np.array([self.y0])
        func = lambdify((x,y),self.dy)
        for i in xarr[:-1]:
            ytemp = yarr[-1] + h * func(i,yarr[-1])
            yarr = np.append(yarr, ytemp)
        return yarr

    # Grafica de la solucion por el metodo de Euler
    def grapheuler(self):
        arrx = self.arrX()
        yeul = self.inteuler()
        plt.figure(figsize=(10,8))
        plt.plot(arrx, yeul , label="Solucion integrada por metodo de Euler",color="red")
        plt.grid()
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.savefig("solucion_euler_ode.png")

    # Grafica de la solucion analitica y numerica
    def comparesols(self):
        y = Function('y')
        if self.exactsol().rhs.has(symbols('y')): 
            raise Exception("El sistema no se pudo resolver el sistema de manera explicita")
        excsol = lambdify(x, self.exactsol().rhs)
        arrx = self.arrX()
        yeul = self.inteuler()
        yexc = excsol(arrx)
        plt.figure(figsize=(10,8))
        plt.plot(arrx, yeul , label="Solucion integrada por metodo de Euler",color="red")
        plt.plot(arrx, yexc , label="Solucion analitica",color="blue")
        plt.grid()
        plt.xlabel("x")
        plt.ylabel("y")
        plt.legend()
        plt.savefig("comparacion_soluciones_ode.png")
