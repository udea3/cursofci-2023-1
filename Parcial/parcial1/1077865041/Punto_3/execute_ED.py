import pendulo_simple as ps
import numpy as np


#Funciones
vector=[1,0]
def equation(v, t):
    f1 = v[1]
    f2 = -9.8/0.625 * v[0]
    return [f1, f2]

def f1(theta,t):
    g=9.8
    l = 0.625
    return -g/l*(theta)
def f2(omega,t):
    return omega
#Función test
def f3(x,y):
    return np.exp(-x)
    
if __name__ =='__main__':
    print("Veamos las soluciones pedidas:")

    #Test
    b=ps.sol_gen(-1,0.01,1,f3)
    b.graficar()

    #Pendulo
    a=ps.pendulo_Simple(1,0,0.01,5,f1,f2,equation,vector)
    a.euler()
    a.graficar2()
    a.graficar3()
